import type Product from "@/types/Product";
import http from "./axios";
function getProducts() {
  return http.get("/products");
}
function saveProduct(products: Product) {
  return http.post("/products", products);
}
function updateProduct(id: number, products: Product) {
  return http.patch(`/products/${id}`, products);
}
function daleteProduct(id: number) {
  return http.delete(`/products/${id}`);
}
export default { getProducts, saveProduct, updateProduct, daleteProduct };
